import java.util.Stack;

public class ValidParentheses_20 {
    public boolean isValid(String s) {

        Stack<Character> characterStack = new Stack<>();

        if(s.length()==1)return false;

        for (int i = 0; i < s.length(); i++) {

            switch (s.charAt(i)){
                case '(':{
                    characterStack.push('(');
                    break;
                }
                case ')':{
                    if(characterStack.isEmpty()) return false;
                    char v = characterStack.pop();
                    if(v!='(') return false;
                    break;
                }
                case '{':{
                    characterStack.push('{');
                    break;
                }
                case '}':{
                    if(characterStack.isEmpty()) return false;
                    char v = characterStack.pop();
                    if(v!='{') return false;
                    break;
                }
                case '[':{
                    characterStack.push('[');
                    break;
                }
                case ']':{
                    if(characterStack.isEmpty()) return false;
                    char v = characterStack.pop();
                    if(v!='[') return false;
                    break;
                }
            }

        }
        return characterStack.isEmpty();



    }
}
