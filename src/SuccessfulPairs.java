import java.util.Arrays;

public class SuccessfulPairs {

    public int firstPosition(int spell, int[] potions, long success){
        int position = -1;
        int low = 0;
        int high = potions.length-1;
        while (low<=high){
            int mid = (low+high)/2;
            if((long) potions[mid] *spell>=success){
                position = mid;
                high = mid-1;
            }else{
                low = mid+1;
            }
        }

        if(position==-1){
            return potions.length;
        }
        return position;
    }


    public int[] successfulPairs(int[] spells, int[] potions, long success) {

        int[] res = new int[spells.length];
        Arrays.sort(potions);

        for (int i = 0;i<spells.length;i++){
            res[i] = potions.length - firstPosition(spells[i], potions, success);
        }


        return res;

    }

}
