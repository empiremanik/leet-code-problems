public class ContainerWithMostWater_11 {
    public int maxArea(int[] height) {

        int low = 0;
        int high = height.length-1;
        int res = 0;

        while (low<high){
            int temp;
            if(height[low] < height[high]){
                temp = height[low] * (high-low);
                low++;
            }else{
                temp = height[high] * (high-low);
                high--;
            }

            if(temp>res){
                res = temp;
            }

        }

        return res;

    }
}
