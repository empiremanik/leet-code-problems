import java.util.HashMap;
import java.util.Map;

public class TowSum_1 {
    public static int[] twoSum(int[] suns, int target) {
        int[] res = new int[2];
        Map<Integer, Integer> map = new HashMap<>();

        for(int i = 0;i<suns.length;i++){
            int val = target - suns[i];
            if(map.containsKey(val)){
                res[0] = map.get(val);
                res[1] = i;
            }else {
                map.put(suns[i],i);
            }
        }


        return res;

    }

}
