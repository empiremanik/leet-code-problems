import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class RemoveDuplicatesFromSortedArray_26 {

    public int removeDuplicates(int[] nums) {
        int count = 1;
        for (int i = 0; i < nums.length-1; i++) {
            if(nums[i]<nums[i+1]){
                nums[count] = nums[i+1];
                count++;
            }
        }
        return count;
    }

}
