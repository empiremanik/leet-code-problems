import java.util.Arrays;

public class BoatsToSavePeople_881 {

    public int numRescueBoats(int[] people, int limit) {


        int sum = 0;
        int low = 0;
        int heigh = people.length - 1;
        Arrays.sort(people);
        while(low<=heigh){
            sum++;
            if( people[low]+people[heigh]<=limit){
                low++;
            }
            heigh--;
        }

        return sum;
    }

}
