import java.util.Map;

public class RomanToInteger {

    public  int romanToInt(String s){
        int res = 0;
        Map<Character,Integer> map = Map.of(
                'M',1000,
                'D',500,
                'C',100,
                'L',50,
                'X',10,
                'V',5,
                'I',1
        );

        for(int i = 0;i<s.length();i++){
            if(i==s.length()-1){
                res += map.get(s.charAt(i));
                break;
            }
            char a1 = s.charAt(i);
            char a2 = s.charAt(i+1);
            if(map.get(a1)<map.get(a2)){
                res -= map.get(a1);
            }else{
                res += map.get(a1);
            }
        }

        return res;
    }

}
