public class FindTheIndexOfTheFirstOccurrenceInAString_28 {
    public int strStr(String haystack, String needle) {

        for (int i = 0; i < haystack.length(); i++) {
            if(i+needle.length()<=haystack.length()){
                String sub = haystack.substring(i,needle.length()+i);
                if(needle.equals(sub)){
                    return i;
                }
            }else{
                break;
            }

        }

        return  -1;

    }

}
