public class LengthOfLastWord_58 {
    public int lengthOfLastWord(String s) {

        String newS = s.trim();
        String[] split = newS.split(" ");

        return split[split.length-1].length();


    }
}
